# Fluent Plugin for slapo-auditlog

## Overview

[Fluentd](http://fluentd.org/) input plugin
that reads events from [slapo-auditlog](http://linux.die.net/man/5/slapo-auditlog).

## Installation

This project has not been published in rubygems.org at this time,
so you have to build and install by yourself.
Your need [bundler](http://bundler.io/) to install.

In case of using with Fluentd:
Fluentd will be also installed via the process below.

    git clone https://github.com/bjhays/fluent-plugin-tail-ldif
    cd fluent-plugin-tail-ldif
    bundle install
    rake build
    rake install

Also, you can use this plugin with [td-agent](https://github.com/treasure-data/td-agent):
You have to install td-agent before installing this plugin.

    git clone https://github.com/bjhays/fluent-plugin-tail-ldif
    cd fluent-plugin-tail-ldif
    bundle install
    rake build
    fluent-gem install pkg/fluent-plugin-tail-ldif

## Dependencies

 * Ruby 1.9.3+
 * Fluentd 0.10.43+

## Basic Usage

Here are general procedures for using this plugin:

 1. Install.
 1. Edit configuration
 1. Run Fluentd or td-agent

You can run this plugin with Fluentd as follows:

 1. Install.
 1. Edit configuration file and save it as 'fluentd.conf'.
 1. Then, run `fluentd -c /path/to/fluentd.conf`

To run with td-agent, it would be as follows:

 1. Install.
 1. Edit configuration file provided by td-agent.
 1. Then, run or restart td-agent.

## Configuration

Here are items for Fluentd configuration file.

### type

Use 'tail_ldif'.

### debug
Boolean. Enable if you need to debug. Default is false.

## Configuration examples
