# -*- encoding: utf-8 -*-
$:.push File.expand_path("../lib", __FILE__)

Gem::Specification.new do |gem|
  gem.authors       = ["Brad Hays"]
  gem.email         = ["bjhays@gmail.com"]
  gem.description   = %q{Extended tail input plugin for ldif}
  gem.summary       = %q{Extended tail input plugin for slapo-auditlog}
  gem.homepage      = "https://bitbucket.org/bjhays/fluent-plugin-tail-ldif"
  gem.license       = "Apache 2.0"

  gem.files         = `git ls-files`.split($\)
  gem.executables   = gem.files.grep(%r{^bin/}).map{ |f| File.basename(f) }
  gem.test_files    = gem.files.grep(%r{^(test|spec|features)/})
  gem.name          = "in_fluent-plugin-tail-ldif"
  gem.require_paths = ["lib"]
  gem.version       = "0.1.1"

  gem.add_runtime_dependency 'fluentd', '~> 0.10.17'
  gem.add_development_dependency 'rake'
end